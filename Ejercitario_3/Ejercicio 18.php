<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Mi Script PHP">
		<title>Mi script PHP</title>
	</head>
	<body>
		<?php
            require 'funciones/abrir_archivo.php';

            function imprimir_visitas()
            {
                $dir      = "archivos/contador.txt";
                $gestor = abrir_archivo($dir);

                if( $gestor == -1 )
                {
                    echo "No se puede abrir el archivo.";
                }
            else {
                $cant_visitas = (integer)fgets($gestor);
                rewind($gestor);
                $cant_visitas += 1;
                fwrite($gestor, $cant_visitas);
                fclose($gestor);
                echo "Total visitas: $cant_visitas.";
            }
            }
            imprimir_visitas();
		?>
	</body>
</html>
