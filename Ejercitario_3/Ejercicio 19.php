<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Mi Script PHP">
		<title>Mi script PHP</title>
	</head>
	<body>
		<?php
            require 'funciones/abrir_archivo.php';
            require 'funciones/imprimir_archivo.php';

            function cargar_notas($dir_lista,$dir_notas)
            {
                $gestor_lista = abrir_archivo($dir_lista);
                $gestor_notas = abrir_archivo($dir_notas);

                if( ($gestor_lista == -1) or ($gestor_notas == -1) )
                    die('No se puede abrir archivo.');

                $notas = file($dir_lista);
                foreach ( $notas as $value )
                {
                    list($matricula,$nombre,$apellido,$puntaje1,$puntaje2,$puntaje3) = explode(" ", $value);
                    $acumulado = (int)$puntaje1 + (int)$puntaje2 + (int)$puntaje3;
                    $cadena = $matricula ." ". $acumulado."\n";
                    fwrite($gestor_notas, $cadena, strlen($cadena));
                }

                fclose($gestor_notas);
                fclose($gestor_lista);

            }

            $dir_lista   = "archivos/lista_puntajes.txt";
            $dir_notas = "archivos/notas.txt";
            cargar_notas($dir_lista,$dir_notas);
            imprimir_archivo($dir_lista);
            imprimir_archivo($dir_notas);
		?>
	</body>
</html>


