<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Mi Script PHP">
		<title>Mi script PHP</title>
	</head>
	<body>
		<?php
            echo '
             <h3>Ingrese los siguientes datos</h3>
            <div class = "container">
                <form role="form" method="POST" action="Ejercicio 21.php">
                    <input type = "text" name = "nombreApellido" placeholder = "Nombre y Apellido" required autofocus>
                    <br><br>
                    <input type = "password" name = "contrasena" placeholder = "contraseña" required>
                    <br>
                    <p>Seleccione su nacionalidad</p>
                    <select name="nacionalidad" id="nacionalidad">
                    <option value=""></option>   
                    <option value="Paraguaya">Paraguaya</option>
                    <option value="Argentina">Argentina</option>
                    <option value="Uruguaya">Uruguaya</option>
                    <option value="Brasilera">Brasilera</option>
                    </select>
                    <br>
                    <p>Seleccione sus niveles educativos:</p>
                    <input type="checkbox" id="primaria" name="primaria" value="primaria">
                    <label for="Primaria">Primaria</label><br>
                    <input type="checkbox" id="secundaria" name="secundaria" value="secundaria">
                    <label for="Secundaria">Secundaria</label><br>
                    <input type="checkbox" id="terciaria" name="terciaria" value="terciaria">
                    <label for="terciaria">Terciaria</label><br>
                    <input type="checkbox" id="universitaria" name="universitaria" value="universitaria">
                    <label for="universitaria">Universitaria</label><br>
                    <input type="checkbox" id="postgrado" name="postgrado" value="postgrado">
                    <label for="postgrado">Postgrado</label><br>
                    <p>Seleccione su genero:</p>
                    <input type="radio" id="masculino" name="genero" value="masculino">
                    <label for="masculino">Masculino</label><br>
                    <input type="radio" id="femenino" name="genero" value="femenino">
                    <label for="femenino">Femenino</label><br>
                    <p>Ingrese una observación si lo desea</p>
                    <label for="observacion"></label>
                    <textarea id="observacion" name="observacion" rows="4" cols="50"></textarea>
                    <br><br>
                    <button type = "submit" name = "guardar-datos">Guardar</button>
                </form>
            </div>             
            ';
            $nombreApellido = $_POST['nombreApellido'];
            $contrasena = $_POST['contrasena'];
            $nacionalidad = $_POST['nacionalidad'];
            $primaria = $_POST['primaria'];
            $secundaria = $_POST['secundaria'];
            $terciaria = $_POST['terciaria'];
            $universitaria = $_POST['universitaria'];
            $postgrado = $_POST['postgrado'];
            $genero = $_POST['genero'];
            $observacion = $_POST['observacion'];

            if($nombreApellido&&$contrasena&&$nacionalidad&&$primaria&&$genero)
            {
            echo "Datos personales ingresados:";
            echo '<br><br>';

            echo "Nombre y apellido: ".$nombreApellido;
            echo '<br>';

            echo "Contraseña: ".$contrasena;
            echo '<br>';

            echo "Nacionalidad: ".$nacionalidad;
            echo '<br>';

            if ($primaria&&$secundaria&&$terciaria&&$universitaria&&$postgrado) {
                echo "Educación: ".$primaria."-".$secundaria."-".$terciaria."-".$universitaria."-".$postgrado;
            }elseif ($primaria&&$secundaria&&$terciaria&&$universitaria){
                echo "Educación: ".$primaria."-".$secundaria."-".$terciaria."-".$universitaria;
            }elseif ($primaria&&$secundaria&&$terciaria){
                echo "Educación: ".$primaria."-".$secundaria."-".$terciaria;
            }elseif ($primaria&&$secundaria){
                echo "Educación: ".$primaria."-".$secundaria;
            }elseif ($primaria){
                echo "Educación: ".$primaria;
            }
            echo '<br>';

            echo "Género: ".$genero;
            echo '<br>';

            echo "Observacion: ".$observacion;
            }else{
            echo "Debe ingresar los datos obligatorios: Nombre y apellido, Contraseña, Nacionalidad, Educación, Género";
            }
		?>
	</body>
</html>

