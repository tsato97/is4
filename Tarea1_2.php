<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Mi segundo script PHP">
		<title>Mi primer script PHP</title>
    <style type="text/css">
      .tabla td {
        text-align: center;
        text-align: center;
        border: 1px solid;
        margin: 3px;
        padding: 3px;
      }
      .tabla {
        border-collapse: collapse;
      }
      tr:nth-child(even) {
        background-color: #f2f2f2;
      } 
    </style>
	</head>
	<body>
		<?php
			echo '
			<table class="tabla">
    		<td colspan="3" style="text-align: center; background-color:yellow">Producctos</td>
  			<tr>
    			<td>Nombre</td>
    			<td style="padding-left:8px;padding-right:8px">Cantidad</td>
    			<td style="padding-left:8px;padding-right:8px">Precio (Gs)</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Coca cola</td>
    			<td>100</td>
    			<td>4500</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Pepsi</td>
    			<td>30</td>
    			<td>4800</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Sprite</td>
    			<td>20</td>
    			<td>4500</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Guarana</td>
    			<td>200</td>
    			<td>4500</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">SevenUp</td>
    			<td>24</td>
    			<td>4800</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Fanta Naranja</td>
    			<td>56</td>
    			<td>4800</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Fanta Guarana</td>
    			<td>89</td>
    			<td>4800</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Fanta Naranja</td>
    			<td>10</td>
    			<td>4500</td>
  			</tr>
  			<tr>
    			<td style="text-align: left">Fanta piña</td>
    			<td>2</td>
    			<td>4500</td>
  			</tr>
			</table>


		';
		?>
	</body>
</html>
